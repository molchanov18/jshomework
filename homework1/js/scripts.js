let userName = prompt("Enter your name:")

while (userName === "" || userName === " " || !userName || !isNaN(userName)) {
    userName = prompt("Enter a valid name")
}
let userAge = +prompt("Enter your age")

while (userAge <= 0 || isNaN(userAge)) {
    userAge = prompt("Enter the correct age:")
}
if (userAge < 18) {
    alert("You are not allowed to enter this site.")
}
else if (userAge <= 22) {
    if (confirm("Are you sure you want to continue?")) {
        alert(`Welcome, ${userName}`)
    } else {
        alert("You are not allowed to enter this site.")
    }
}
if (userAge > 22) {
    alert(`Welcome, ${userName}`)
} 