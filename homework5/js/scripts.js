function createNewUser() {
    const userName = prompt("Введите ваше имя")

    const userLastName = prompt("Введите вашу фамилию")

    const userBirthday = prompt("Введите вашу дату рождения в формате дд.мм.гггг")
    
    const newUser = {
        name: userName,
        lastName: userLastName,
        birthday: userBirthday,  
        getLogin: function () {
            const userLogin = this.name[0].toLowerCase() + this.lastName.toLowerCase();
            return userLogin;
        },
        getAge: function () {
            const now = new Date();
            const year = this.birthday.slice(6);
            let month = this.birthday.slice(3,5);
            month--;
            const day = this.birthday.slice(0,2);
            let age = now.getFullYear() - year;
            if(month > now.getMonth() || (month === now.getMonth() && day > now.getDate())) {
                age--;  
            }
            return age;
        },
        getPassword: function () {
            const userPassword = this.name[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(6);
            return userPassword;
        }
    }
    return newUser;
}

const createUser = createNewUser();
console.log(createUser);
console.log(createUser.getLogin());
console.log(createUser.getAge()) ;
console.log(createUser.getPassword());