const tabs = document.querySelector(".tabs");
const tabsContent = document.querySelector(".tabs-content");
const tabsChildren = tabs.children;
const tabsContentChildren = tabsContent.children;

for(let i = 0; i < tabsChildren.length; i++){
    tabsChildren[i].dataset.index = String(i);
    
    if(i !== 0){
        tabsContentChildren[i].hidden = true;
    }
}

tabs.addEventListener("click", () => {
    tabs.querySelector(".active").classList.remove("active")
    tabsContent.querySelector("li:not([hidden])").hidden = true;
    event.target.classList.add("active")
    tabsContentChildren[event.target.dataset.index].hidden = false;
})



