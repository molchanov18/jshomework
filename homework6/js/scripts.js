const thisArr = ["string", 12, NaN, "Hello, World!", 111]

function filterBy(arr, elem) {
    const newArr = arr.filter(item => typeof item !== elem)
    return newArr
}

const noNumArr = filterBy(thisArr, "number")

console.log(noNumArr)

const noStrArr = filterBy(thisArr, "string")

console.log(noStrArr)